# Makefile twentyseconds cv

files_tex = $(wildcard *.tex)
all: pdf
	@echo "Done!"
pdf: *.tex
	@echo "Building.... $^"
	@$(foreach var,$(files_tex),pdflatex -interaction=nonstopmode '$(var)' 1>/dev/null;)
clean:
	@rm -f *.aux *.dvi *.log *.out *.pdf *.bak *.fdb_latexmk *.fls
	@echo "Clean done.";