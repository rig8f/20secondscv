Twenty Seconds Curriculum Vitae in LaTeX
========================================

_Please note that this is an indirect fork of [the original](https://github.com/spagnuolocarmine/TwentySecondsCurriculumVitae-LaTex) template, further modified by [LaTeXTemplates.com](http://www.LaTeXTemplates.com). I added some modifications to that second version and also translated it into italian. So this is the original README, from the original author, modified with added commands [marked as italic]._

Non c'è la versione italiana di questo file, ma _template-it.tex_ contiene estesi commenti per guidare alla compilazione.

# Curricula Vitae - Résumés
A curriculum vitae, otherwise known as a CV or résumé, is a document used by individuals to communicate their work history, education and skill set. This is a style template for your curriculum written in LaTex. The main goal of this template is to provide a curriculum that is able to survive to the résumés screening of "twenty seconds".

_The author assumes no responsibility for the topicality, correctness, completeness or quality of the information provided and for the obtained résumés._

This is designed for computer scientists but there is no limitation to use it for résumés in other disciplines.

##The basic idea is KISS - Keep It Simple, Stupid.

In a nutshell _**"It is vain to do with more what can be done with fewer"**_ -- Occam's razor --

* **This template has been designed to create a "one-page" résumé is therefore not suitable to create curriculum of more than one-page.** 

* _Please do not try to create curriculum more than one-page._ 

###How to describe your experiences?

There are many theories about the résumé screening process of "Big" companies.
Resume screeners and the interviewers look in your résumé for:

- Are you smart?
- Can you code (act for what you apply)?

Anyway according to the guidelines of this template you should use a really simple form to describe each items in your résumé: 

	Accomplished <X> by implementing <Y> which led to <Z>

Here's an examples:
	
	Reduced object rendering time by 75% by applying Floyd's algorithm, leading to a 10% reduction in system boot time.
	
-- _Cracking the Coding Interview, Book, Gayle Laakmann Mcdowell_ --

##Toy Résumé (Original template)
***
![sample résumé](https://github.com/spagnuolocarmine/TwentySecondsCurriculumVitae-LaTex/raw/master/Twenty-Seconds_cv.jpg)
***

##Toy Résumé (This template)
***
![sample résumé](https://bytebucket.org/rig8f/20secondscv/raw/89addb77d7c25b3378e77af139973139a8cc6b67/template-en.jpg)
***

# Build 
This guide walks you to build your résumé.

Build requirements:

* LaTex installation.
	* additionals packages:	 	
		- ClearSans, fontenc
		- tikz
		- xcolor
		- textpos
		- etoolbox
		- ifthen
		- pgffor
		- parskip
		- _iflang_
		- _fontawesome_
		- _hyperxmp_

###Build through GNU Make command
Clean your project résumé.
	
	make clean
	
Build your project résumé.

	make all
	
-- _Alternately you can build through your favorite LaTex editor._ --

#Environment style and list of commands

The style is divided in two parts. The former is the left side bar: that contains personal information, profile picture, and information about your professional skills. The second part is the body that should be contains details about your academic studies, professional experiences and all the information that you want (remember the KISS principle).

###Profile environment
These are the command to set up the profile information.

* Set up the image profile.
	
		\profilepic{paht_name}

* Set up your name.
	
		\cvname{your name}

* Set up your job profile.
	
		\cvjobtitle{your job title}

* Set up your date of birth.
	
		\cvdate{date}	

* Set up your address.
	
		\cvaddress{address}		

* Set up your telephone number.
	
		\cvphone{phone number}

* _Set up your telegram username._
	
		\cvtelegram{telegram username}

* Set up your personal home page.
	
		\cvsite{home page address}

* Set up your email.
	
		\cvmail{email address}

* _Set up your Skype ID._
	
		\cvskype{Skype ID}

* _Set up your Facebook username._
	
		\cvfacebook{username}

* _Set up your Twitter username._
	
		\cvtwitter{username}

* _Set up your Google plus home page._
	
		\cvgplus{home page address}

* _Set up your Wordpress / Gravatar ID._
	
		\cvwordpress{Wordpress ID}

* _Set up your Youtube username._
	
		\cvyoutube{username}

* _Set up your Vimeo username._
	
		\cvvimeo{username}

* _Set up your GitHub home page._
	
		\cvgithub{home page address}

* _Set up your Bitbucket home page._
	
		\cvgitbb{home page address}

* _Set up your GitLab home page._
	
		\cvgitlab{home page address}

* Set up a brief description of you.
	
		\aboutme{brief description}

* Set up the skills with chart style. Each skill must is a couple `{name/value}`, where the value is a floating point value between `0` and `6`. This is an agreement for the graphics issues, the `0` correspond to a Fundamental awareness while `6` to a Expert awareness level. _Disable skills sections by passing `noskills` option to documentclass._
	
		\skills{{name skill1/5.8},{name skill2/4}} 

* Set up the skills with text style.
	
		\skillstext{{name skill1/5.8},{name skill2/4}} 

To create the profile use the command:

	\makeprofile

###Body environment
The body document part is composed by sections.
In the sections you can put two kinds of list items.

The first (_Twenty items environment_) intends a list of detailed information with four part: **Data** -- **Title** -- **Place** -- **Description**. 

The second (_Twenty items short environment_) intends a fewer informationinformation (you can customize this list more easily): **Data** -- **Description**.
#### Sections
* Set up a new section in the body part.
		
		\section{sction name}


####Twenty items environment
```
\begin{twenty}
  \twentyitem
    {year}
    {title}
    {place}
    {description}
\end{twenty}
```

#### Twenty items short environment
```
\begin{twentyshort}
  \twentyitemshort
    {year}
    {description}
\end{twentyshort}
```
###Other commands
There other two fun command: \icon and \round; that enables to wrap the text in oval shape.

```
	\icon{text}
	\round{text, color}
```